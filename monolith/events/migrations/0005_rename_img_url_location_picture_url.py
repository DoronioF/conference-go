# Generated by Django 4.0.3 on 2022-08-18 21:48

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0004_alter_location_img_url'),
    ]

    operations = [
        migrations.RenameField(
            model_name='location',
            old_name='img_url',
            new_name='picture_url',
        ),
    ]
